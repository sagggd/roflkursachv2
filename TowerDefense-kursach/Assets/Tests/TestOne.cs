using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class TestOne
{
    [Test]
    public void TestEquals()
    {
        int num1 = 1;
        int num2 = 1;
        Assert.That(num1 == num2);
    }
    [Test]
    public void TestRun()
    {
        MoveEnemy enemy;
        GameObject gameObject = new GameObject("Enemy");
        enemy = gameObject.AddComponent<MoveEnemy>();
        
        Assert.AreEqual(1.0f, enemy.speed);
    }

    [Test]
    public void TestHealth()
    {
        GameManagerBehavior health;
        GameObject gameObject = new GameObject("health");
        health = gameObject.AddComponent<GameManagerBehavior>();
        Assert.AreEqual(5,health.Health);
    }
}
